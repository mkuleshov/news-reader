package ru.mk.newsreader.service.sync

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import ru.mk.newsreader.data.converter.RemoteNewsConverter
import ru.mk.newsreader.data.interactor.LocalNewsInteractor
import ru.mk.newsreader.data.interactor.RemoteNewsInteractor
import ru.mk.newsreader.rx.SchedulersFactory
import timber.log.Timber

interface SyncPresenter {

    fun startSync()

    fun destroy()

}

class SyncPresenterImpl(private val schedulers: SchedulersFactory,
                        private val localInteractor: LocalNewsInteractor,
                        private val remoteInteractor: RemoteNewsInteractor,
                        private val remoteConverter: RemoteNewsConverter) : SyncPresenter {

    private val disposables = CompositeDisposable()


    override fun startSync() {
        Timber.d("Start sync news feed...")
        disposables += remoteInteractor.getNews()
                .subscribeOn(schedulers.io())
                .subscribe({
                    localInteractor.saveNews(it.map { remoteConverter.convert(it) })
                    Timber.d("Syncing news feed completed!")
                }, {
                    Timber.e(it, "Error on sync news feed")
                })
    }

    override fun destroy() {
        disposables.clear()
    }

}