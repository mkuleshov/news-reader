package ru.mk.newsreader.service.sync

import com.firebase.jobdispatcher.JobParameters
import com.firebase.jobdispatcher.JobService
import dagger.android.AndroidInjection
import javax.inject.Inject

class SyncJobService : JobService() {

    @Inject
    lateinit var presenter: SyncPresenter

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onStartJob(job: JobParameters?): Boolean {
        presenter.startSync()
        return true
    }

    override fun onStopJob(job: JobParameters?): Boolean {
        return true
    }

    override fun onDestroy() {
        presenter.destroy()
        super.onDestroy()
    }
}