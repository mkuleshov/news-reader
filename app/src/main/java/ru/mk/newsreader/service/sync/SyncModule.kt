package ru.mk.newsreader.service.sync

import dagger.Module
import dagger.Provides
import ru.mk.newsreader.data.api.Api
import ru.mk.newsreader.data.converter.RemoteNewsConverter
import ru.mk.newsreader.data.converter.RemoteNewsConverterImpl
import ru.mk.newsreader.data.db.dao.NewsDao
import ru.mk.newsreader.data.interactor.LocalNewsInteractor
import ru.mk.newsreader.data.interactor.LocalNewsInteractorImpl
import ru.mk.newsreader.data.interactor.RemoteNewsInteractor
import ru.mk.newsreader.data.interactor.RemoteNewsInteractorImpl
import ru.mk.newsreader.di.annotation.ServiceScope
import ru.mk.newsreader.rx.SchedulersFactory

@Module
class SyncModule {

    @Provides
    @ServiceScope
    fun provideRemoteInteractor(api: Api): RemoteNewsInteractor {
        return RemoteNewsInteractorImpl(api)
    }

    @Provides
    @ServiceScope
    fun provideLocalInteractor(newsDao: NewsDao): LocalNewsInteractor {
        return LocalNewsInteractorImpl(newsDao)
    }

    @Provides
    @ServiceScope
    fun provideRemoteConverter(): RemoteNewsConverter {
        return RemoteNewsConverterImpl()
    }

    @Provides
    @ServiceScope
    fun provideSyncPresenter(schedulers: SchedulersFactory,
                             localInteractor: LocalNewsInteractor,
                             remoteInteractor: RemoteNewsInteractor,
                             remoteConverter: RemoteNewsConverter): SyncPresenter {
        return SyncPresenterImpl(schedulers, localInteractor, remoteInteractor, remoteConverter)
    }

}