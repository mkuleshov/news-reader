package ru.mk.newsreader.data.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import ru.mk.newsreader.data.db.entity.NewsEntity

@Dao
interface NewsDao {

    @Query("SELECT * FROM news ORDER BY pub_date DESC")
    fun getNews(): Flowable<List<NewsEntity>>

    @Insert(onConflict = REPLACE)
    fun insertNews(news: NewsEntity)

    @Query("DELETE FROM news")
    fun deleteAll()

}