package ru.mk.newsreader.data.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "Rss", strict = false)
data class Rss(
        @field:Attribute(name = "version") var version: String = "",
        @field:Element(name = "channel") var channel: Channel = Channel())