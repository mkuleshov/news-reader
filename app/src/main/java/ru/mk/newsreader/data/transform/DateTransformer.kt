package ru.mk.newsreader.data.transform

import org.simpleframework.xml.transform.Transform
import java.text.SimpleDateFormat
import java.util.*

class DateTransformer : Transform<Date> {

    val DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss zzz"

    private val formatter: SimpleDateFormat = SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH)

    override fun write(value: Date?): String {
        return formatter.format(value)
    }

    override fun read(value: String?): Date {
        return formatter.parse(value)
    }

}