package ru.mk.newsreader.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import ru.mk.newsreader.data.db.converter.Converters
import ru.mk.newsreader.data.db.dao.NewsDao
import ru.mk.newsreader.data.db.entity.NewsEntity

@Database(entities = [(NewsEntity::class)], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun newsDao(): NewsDao

}