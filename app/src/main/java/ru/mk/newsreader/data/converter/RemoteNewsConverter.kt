package ru.mk.newsreader.data.converter

import ru.mk.newsreader.data.db.entity.NewsEntity
import ru.mk.newsreader.data.model.Channel

interface RemoteNewsConverter {

    fun convert(item: Channel.Item): NewsEntity

}

class RemoteNewsConverterImpl : RemoteNewsConverter {

    override fun convert(item: Channel.Item): NewsEntity {
        return NewsEntity(
                item.guid,
                item.pubDate,
                item.title,
                item.link,
                item.description
        )
    }

}