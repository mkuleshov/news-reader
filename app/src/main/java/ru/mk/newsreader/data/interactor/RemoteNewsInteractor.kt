package ru.mk.newsreader.data.interactor

import io.reactivex.Observable
import ru.mk.newsreader.data.api.Api
import ru.mk.newsreader.data.model.Channel

interface RemoteNewsInteractor {

    fun getNews(): Observable<List<Channel.Item>>

}

class RemoteNewsInteractorImpl(private val api: Api) : RemoteNewsInteractor {

    override fun getNews(): Observable<List<Channel.Item>> {
        return api.fetchNews()
                .map { it.channel.items }
    }

}