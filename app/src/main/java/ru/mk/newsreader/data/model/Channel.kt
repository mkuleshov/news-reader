package ru.mk.newsreader.data.model

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root
import java.util.*


@Root(name = "channel", strict = false)
data class Channel(
        @field:ElementList(name = "item", required = true, inline = true)
        var items: List<Item> = arrayListOf()
) {

    @Root(name = "item", strict = false)
    data class Item(
            @field:Element(name = "guid", required = true)
            var guid: String = "",
            @field:Element(name = "pubDate", required = true)
            var pubDate: Date = Date(),
            @field:Element(name = "title", required = true)
            var title: String = "",
            @field:Element(name = "link", required = true)
            var link: String = "",
            @field:Element(name = "description", required = true)
            var description: String = ""
    )

}