package ru.mk.newsreader.data.interactor

import io.reactivex.Flowable
import ru.mk.newsreader.data.db.dao.NewsDao
import ru.mk.newsreader.data.db.entity.NewsEntity

interface LocalNewsInteractor {

    fun getNews(): Flowable<List<NewsEntity>>

    fun saveNews(news: List<NewsEntity>)

}

class LocalNewsInteractorImpl(private val newsDao: NewsDao): LocalNewsInteractor {

    override fun saveNews(news: List<NewsEntity>) {
        news.forEach {
            newsDao.insertNews(it)
        }
    }

    override fun getNews(): Flowable<List<NewsEntity>> {
        return newsDao.getNews()
    }

}