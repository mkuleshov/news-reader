package ru.mk.newsreader.data.model

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import java.util.*

@ElementList(name = "item")
@Deprecated("Use Channel instead")
class News(
        @Element(name = "guid") guid: String,
        @Element(name = "pubDate") pubDate: Date,
        @Element(name = "title") title: String,
        @Element(name = "link") link: String,
        @Element(name = "description") description: String)