package ru.mk.newsreader.data.api

import io.reactivex.Observable
import retrofit2.http.GET
import ru.mk.newsreader.data.model.Rss

interface Api {

    @GET("_rss.html?subtype=1&category=2&city=21")
    fun fetchNews(): Observable<Rss>

}