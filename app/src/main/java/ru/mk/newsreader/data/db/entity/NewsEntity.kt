package ru.mk.newsreader.data.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.*

@Entity(tableName = "news")
data class NewsEntity(
        @PrimaryKey var guid: String,
        @ColumnInfo(name = "pub_date") var pubDate: Date,
        @ColumnInfo(name = "title") var title: String,
        @ColumnInfo(name = "link") var link: String,
        @ColumnInfo(name = "description") var description: String)