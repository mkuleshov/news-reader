package ru.mk.newsreader.extension

import android.os.Bundle
import android.support.v4.app.Fragment

fun <T : Fragment> T.arguments(size: Int = -1, init: Bundle.() -> Unit): T {
    val bundle = if (size < 0) Bundle() else Bundle(size)
    bundle.init()
    arguments = bundle
    return this
}