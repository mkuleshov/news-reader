package ru.mk.newsreader.ui.newslist.item

import ru.mk.newsreader.adapter.item.Item
import java.util.*

data class NewsViewItem(
        val guid: String,
        val pubDate: Date,
        val title: String,
        val link: String,
        val description: String): Item