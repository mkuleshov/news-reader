package ru.mk.newsreader.ui.newsdetail

import com.arellomobile.mvp.InjectViewState
import ru.mk.newsreader.data.interactor.LocalNewsInteractor
import ru.mk.newsreader.mvp.BasePresenter
import ru.mk.newsreader.rx.SchedulersFactory
import ru.mk.newsreader.ui.newslist.NewsViewItemConverter
import ru.mk.newsreader.ui.newslist.item.NewsViewItem
import ru.terrakok.cicerone.Router

@InjectViewState
class NewsDetailPresenter(
        private val schedulers: SchedulersFactory,
        private val localNewsInteractor: LocalNewsInteractor,
        private val router: Router,
        private val converter: NewsViewItemConverter
) : BasePresenter<NewsDetailView>() {

    private lateinit var newsItems: List<NewsViewItem>
    private lateinit var currentNewsId: String

    fun initState(newsId: String) {
        currentNewsId = newsId
        viewState.showLoading()
        unsubscribeOnDestroy(localNewsInteractor.getNews()
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({
                    viewState.hideLoading()
                    newsItems = it.map { converter.convert(it) }
                    viewState.displayItems(newsItems)
                    displaySelectedNews()
                }, {
                    viewState.hideLoading()
                }))
    }

    private fun displaySelectedNews() {
        val position = findPositionOfNews()
        val item = newsItems[position]
        viewState.scrollToPosition(position)
        viewState.setTitle(item.title)
    }

    private fun findPositionOfNews(): Int {
        newsItems.forEachIndexed { index, news ->
            if (news.guid == currentNewsId) {
                return index
            }
        }
        return 0
    }

    fun onUpClick() {
        router.exit()
    }

    fun onPageSelected(position: Int) {
        val item = newsItems[position]
        viewState.setTitle(item.title)
    }

}