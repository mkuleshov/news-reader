package ru.mk.newsreader.ui.newsdetail

import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ProgressBar
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ru.mk.newsreader.R
import ru.mk.newsreader.data.interactor.LocalNewsInteractor
import ru.mk.newsreader.extension.arguments
import ru.mk.newsreader.rx.SchedulersFactory
import ru.mk.newsreader.ui.base.BaseFragment
import ru.mk.newsreader.ui.newslist.NewsViewItemConverter
import ru.mk.newsreader.ui.newslist.item.NewsViewItem
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class NewsDetailFragment: BaseFragment(), NewsDetailView {

    @Inject
    lateinit var localNewsInteractor: LocalNewsInteractor
    @Inject
    lateinit var schedulers: SchedulersFactory
    @Inject
    lateinit var router: Router
    @Inject
    lateinit var newsViewItemConverter: NewsViewItemConverter
    @InjectPresenter
    lateinit var presenter: NewsDetailPresenter

    lateinit var toolbar: Toolbar
    lateinit var pager: ViewPager
    lateinit var progress: ProgressBar

    lateinit var pagerAdapter: PagerAdapter

    @ProvidePresenter
    fun providePresenter(): NewsDetailPresenter {
        return NewsDetailPresenter(
                schedulers,
                localNewsInteractor,
                router,
                newsViewItemConverter)
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_news_details
    }

    override fun initControls(v: View) {
        toolbar = v.findViewById(R.id.toolbar)
        pager = v.findViewById(R.id.pager)
        progress = v.findViewById(R.id.view_progress)
        initToolbar()
        initPager()
        loadData()
    }

    private fun initToolbar() {
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
        toolbar.setNavigationOnClickListener {
            presenter.onUpClick()
        }
        toolbar.inflateMenu(R.menu.menu_news_details)
        toolbar.setOnMenuItemClickListener {
            return@setOnMenuItemClickListener when (it.itemId) {
                R.id.action_favorites -> {
                    // Добавить логику добавления в избранное
                    true
                }
                else -> true
            }
        }
    }

    private fun initPager() {
        pager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                presenter.onPageSelected(position)
            }

        })
    }

    private fun loadData() {
        presenter.initState(arguments?.getString(KEY_NEWS_GUID, "") ?: "")
    }

    override fun setTitle(title: String) {
        toolbar.title = title
    }

    override fun showLoading() {
        progress.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progress.visibility = View.GONE
    }

    override fun displayItems(items: List<NewsViewItem>) {
        fragmentManager?.let {
            pagerAdapter = NewsPagerAdapter(it, items)
            pager.adapter = pagerAdapter
        }
    }

    override fun scrollToPosition(position: Int) {
        pager.currentItem = position
    }

}

const val KEY_NEWS_GUID = "news_guid"

fun createNewsDetailFragment(newsGuid: String): NewsDetailFragment {
    return NewsDetailFragment().arguments(1, {
        putString(KEY_NEWS_GUID, newsGuid)
    })
}