package ru.mk.newsreader.ui.newslist.delegate

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.View
import android.widget.TextView
import ru.mk.newsreader.R
import ru.mk.newsreader.adapter.delegate.BaseAdapterDelegate
import ru.mk.newsreader.adapter.item.Item
import ru.mk.newsreader.ui.newslist.item.NewsViewItem
import ru.mk.newsreader.util.DateFormatter

class NewsItemDelegate(context: Context,
                       private val dateFormatter: DateFormatter,
                       private val clickListener: (newsId: String) -> Unit) : BaseAdapterDelegate<NewsViewItem, NewsItemDelegate.NewsHolder>(context) {

    override fun isForViewType(item: Item): Boolean {
        return item is NewsViewItem
    }

    override fun getLayoutId(): Int {
        return R.layout.layout_news_item
    }

    override fun createViewHolder(view: View): NewsHolder {
        return NewsHolder(view)
    }

    override fun onBind(item: NewsViewItem, holder: NewsHolder) {
        holder.itemView.tag = item
        holder.textDate.text = dateFormatter.format(item.pubDate)
        holder.textTitle.text = Html.fromHtml(item.title)
        holder.textDescription.text = Html.fromHtml(item.description)
    }

    inner class NewsHolder(view: View) : RecyclerView.ViewHolder(view) {

        init {
            itemView.setOnClickListener {
                val tag = itemView.tag
                if (tag is NewsViewItem) {
                    clickListener.invoke(tag.guid)
                }
            }
        }

        var textDate: TextView = itemView.findViewById(R.id.text_date)
        var textTitle: TextView = itemView.findViewById(R.id.text_title)
        var textDescription: TextView = itemView.findViewById(R.id.text_description)

    }

}