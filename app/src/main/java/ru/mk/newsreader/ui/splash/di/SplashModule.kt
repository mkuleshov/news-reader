package ru.mk.newsreader.ui.splash.di

import dagger.Module
import dagger.Provides
import ru.mk.newsreader.data.api.Api
import ru.mk.newsreader.data.converter.RemoteNewsConverter
import ru.mk.newsreader.data.converter.RemoteNewsConverterImpl
import ru.mk.newsreader.data.db.dao.NewsDao
import ru.mk.newsreader.data.interactor.LocalNewsInteractor
import ru.mk.newsreader.data.interactor.LocalNewsInteractorImpl
import ru.mk.newsreader.data.interactor.RemoteNewsInteractor
import ru.mk.newsreader.data.interactor.RemoteNewsInteractorImpl
import ru.mk.newsreader.di.annotation.FragmentScope

@Module
class SplashModule {

    @Provides
    @FragmentScope
    fun provideRemoteInteractor(api: Api): RemoteNewsInteractor {
        return RemoteNewsInteractorImpl(api)
    }

    @Provides
    @FragmentScope
    fun provideLocalInteractor(newsDao: NewsDao): LocalNewsInteractor {
        return LocalNewsInteractorImpl(newsDao)
    }

    @Provides
    @FragmentScope
    fun provideNewsConverter(): RemoteNewsConverter {
        return RemoteNewsConverterImpl()
    }

}