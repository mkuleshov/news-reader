package ru.mk.newsreader.ui.newsdetail.webview

import android.annotation.SuppressLint
import android.view.View
import android.webkit.WebView
import ru.mk.newsreader.R
import ru.mk.newsreader.extension.arguments
import ru.mk.newsreader.ui.base.BaseFragment


class NewsDetailWebViewFragment: BaseFragment() {

    lateinit var webview: WebView

    override fun getLayoutRes(): Int {
        return R.layout.fragment_news_details_webview
    }

    override fun initControls(v: View) {
        webview = v.findViewById(R.id.webview)
        initWebView()
        loadData()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        val webSettings = webview.settings
        webSettings.javaScriptEnabled = true
    }

    private fun loadData() {
        webview.loadUrl(arguments?.getString(KEY_NEWS_LINK, "") ?: "")
    }

}

const val KEY_NEWS_LINK = "news_link"

fun createNewsDetailWebViewFragment(newsLink: String): NewsDetailWebViewFragment {
    return NewsDetailWebViewFragment().arguments(1, {
        putString(KEY_NEWS_LINK, newsLink)
    })
}