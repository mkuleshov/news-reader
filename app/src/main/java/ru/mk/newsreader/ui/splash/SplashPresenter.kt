package ru.mk.newsreader.ui.splash

import com.arellomobile.mvp.InjectViewState
import ru.mk.newsreader.data.converter.RemoteNewsConverter
import ru.mk.newsreader.data.interactor.LocalNewsInteractor
import ru.mk.newsreader.data.interactor.RemoteNewsInteractor
import ru.mk.newsreader.mvp.BasePresenter
import ru.mk.newsreader.rx.SchedulersFactory
import ru.mk.newsreader.ui.Screens
import ru.terrakok.cicerone.Router
import timber.log.Timber

@InjectViewState
class SplashPresenter(
        private val localInteractor: LocalNewsInteractor,
        private val remoteInteractor: RemoteNewsInteractor,
        private val schedulers: SchedulersFactory,
        private val router: Router,
        private val converterRemote: RemoteNewsConverter) : BasePresenter<SplashView>() {

    override fun onFirstViewAttach() {
        viewState.showLoading()
        unsubscribeOnDestroy(localInteractor.getNews()
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({
                    if (it.isEmpty()) {
                        startSyncNews()
                    } else {
                        navigateToMain()
                    }
                }, {
                    viewState.hideLoading()
                }))
    }

    private fun startSyncNews() {
        unsubscribeOnDestroy(remoteInteractor.getNews()
                .doOnNext {
                    localInteractor.saveNews(it.map { converterRemote.convert(it) })
                }
                .subscribe({
                    viewState.hideLoading()
                    navigateToMain()
                }, {
                    Timber.e(it, "Error loading news feed")
                    viewState.hideLoading()
                }))
    }

    private fun navigateToMain() {
        router.newRootScreen(Screens.NEWS_LIST_SCREEN)
    }

}