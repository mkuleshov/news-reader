package ru.mk.newsreader.ui.newsdetail.di

import dagger.Module
import dagger.Provides
import ru.mk.newsreader.data.db.dao.NewsDao
import ru.mk.newsreader.data.interactor.LocalNewsInteractor
import ru.mk.newsreader.data.interactor.LocalNewsInteractorImpl
import ru.mk.newsreader.di.annotation.FragmentScope
import ru.mk.newsreader.ui.newslist.NewsViewItemConverter
import ru.mk.newsreader.ui.newslist.NewsViewItemConverterImpl

@Module
class NewsDetailModule {

    @Provides
    @FragmentScope
    fun provideLocalInteractor(newsDao: NewsDao): LocalNewsInteractor {
        return LocalNewsInteractorImpl(newsDao)
    }

    @Provides
    @FragmentScope
    fun provideNewsViewItemConverter(): NewsViewItemConverter {
        return NewsViewItemConverterImpl()
    }

}