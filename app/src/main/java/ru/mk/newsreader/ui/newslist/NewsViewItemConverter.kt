package ru.mk.newsreader.ui.newslist

import ru.mk.newsreader.data.db.entity.NewsEntity
import ru.mk.newsreader.ui.newslist.item.NewsViewItem

interface NewsViewItemConverter {

    fun convert(item: NewsEntity): NewsViewItem

}

class NewsViewItemConverterImpl: NewsViewItemConverter {

    override fun convert(item: NewsEntity): NewsViewItem {
        return NewsViewItem(
                item.guid,
                item.pubDate,
                item.title,
                item.link,
                item.description)
    }

}