package ru.mk.newsreader.ui

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.mk.newsreader.di.annotation.FragmentScope
import ru.mk.newsreader.ui.newsdetail.NewsDetailFragment
import ru.mk.newsreader.ui.newsdetail.di.NewsDetailModule
import ru.mk.newsreader.ui.newsdetail.webview.NewsDetailWebViewFragment
import ru.mk.newsreader.ui.newslist.NewsListFragment
import ru.mk.newsreader.ui.newslist.di.NewsListModule
import ru.mk.newsreader.ui.splash.SplashFragment
import ru.mk.newsreader.ui.splash.di.SplashModule

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector(modules = [SplashModule::class])
    @FragmentScope
    abstract fun splashFragment(): SplashFragment

    @ContributesAndroidInjector(modules = [NewsListModule::class])
    @FragmentScope
    abstract fun newsListFragment(): NewsListFragment

    @ContributesAndroidInjector(modules = [NewsDetailModule::class])
    @FragmentScope
    abstract fun newsDetailFragment(): NewsDetailFragment

    @ContributesAndroidInjector(modules = [])
    @FragmentScope
    abstract fun newsDetailWebViewFragment(): NewsDetailWebViewFragment

}