package ru.mk.newsreader.ui.newsdetail

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import ru.mk.newsreader.ui.newsdetail.webview.createNewsDetailWebViewFragment
import ru.mk.newsreader.ui.newslist.item.NewsViewItem

class NewsPagerAdapter(fragmentManager: FragmentManager,
                       private val newsItems: List<NewsViewItem>): FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        val link = newsItems[position].link
        return createNewsDetailWebViewFragment(link)
    }

    override fun getCount(): Int {
        return newsItems.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return newsItems[position].title
    }

}