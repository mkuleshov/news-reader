package ru.mk.newsreader.ui

class Screens {

    companion object {
        const val SPLASH_SCREEN = "splash_screen"
        const val NEWS_LIST_SCREEN = "news_list_screen"
        const val NEWS_DETAILS_SCREEN = "news_details_screen"
    }

}