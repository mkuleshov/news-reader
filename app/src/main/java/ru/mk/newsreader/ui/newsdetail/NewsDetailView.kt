package ru.mk.newsreader.ui.newsdetail

import com.arellomobile.mvp.MvpView
import ru.mk.newsreader.ui.newslist.item.NewsViewItem

interface NewsDetailView: MvpView {

    fun setTitle(title: String)
    fun showLoading()
    fun hideLoading()
    fun displayItems(items: List<NewsViewItem>)
    fun scrollToPosition(position: Int)

}