package ru.mk.newsreader.ui.splash

import android.view.View
import android.widget.ProgressBar
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ru.mk.newsreader.R
import ru.mk.newsreader.data.converter.RemoteNewsConverter
import ru.mk.newsreader.data.interactor.LocalNewsInteractor
import ru.mk.newsreader.data.interactor.RemoteNewsInteractor
import ru.mk.newsreader.rx.SchedulersFactory
import ru.mk.newsreader.ui.base.BaseFragment
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class SplashFragment : BaseFragment(), SplashView {

    @Inject
    lateinit var localNewsInteractor: LocalNewsInteractor
    @Inject
    lateinit var remoteNewsInteractor: RemoteNewsInteractor
    @Inject
    lateinit var schedulers: SchedulersFactory
    @Inject
    lateinit var router: Router
    @Inject
    lateinit var remoteNewsConverter: RemoteNewsConverter
    @InjectPresenter
    lateinit var presenter: SplashPresenter

    private lateinit var progress: ProgressBar

    @ProvidePresenter
    fun providePresenter(): SplashPresenter {
        return SplashPresenter(
                localNewsInteractor,
                remoteNewsInteractor,
                schedulers,
                router,
                remoteNewsConverter)
    }

    override fun getLayoutRes(): Int = R.layout.fragment_splash

    override fun initControls(v: View) {
        progress = v.findViewById(R.id.view_progress)
    }

    override fun showLoading() {
        progress.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progress.visibility = View.GONE
    }


}