package ru.mk.newsreader.ui.newslist

import android.os.Bundle
import com.arellomobile.mvp.InjectViewState
import ru.mk.newsreader.data.converter.RemoteNewsConverter
import ru.mk.newsreader.data.interactor.LocalNewsInteractor
import ru.mk.newsreader.data.interactor.RemoteNewsInteractor
import ru.mk.newsreader.mvp.BasePresenter
import ru.mk.newsreader.rx.SchedulersFactory
import ru.mk.newsreader.ui.Screens
import ru.mk.newsreader.ui.newsdetail.KEY_NEWS_GUID
import ru.terrakok.cicerone.Router

@InjectViewState
class NewsListPresenter(
        private val schedulers: SchedulersFactory,
        private val localNewsInteractor: LocalNewsInteractor,
        private val remoteNewsInteractor: RemoteNewsInteractor,
        private val router: Router,
        private val remoteConverter: RemoteNewsConverter,
        private val viewItemConverter: NewsViewItemConverter) : BasePresenter<NewsListView>() {

    override fun onFirstViewAttach() {
        viewState.showTitle()
        reloadNewsFromDatabase()
    }

    private fun reloadNewsFromDatabase() {
        viewState.showLoading()
        unsubscribeOnDestroy(localNewsInteractor.getNews()
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({
                    viewState.hideLoading()
                    viewState.displayItems(it.map { viewItemConverter.convert(it) })
                }, {
                    viewState.hideLoading()
                    viewState.showError("Ошибка при загрузке новостей")
                }))
    }

    fun onNewsSelected(newsId: String) {
        router.navigateTo(Screens.NEWS_DETAILS_SCREEN, Bundle(1).apply { putString(KEY_NEWS_GUID, newsId) })
    }

    fun onActionSync() {
        viewState.showLoading()
        unsubscribeOnDestroy(remoteNewsInteractor.getNews()
                .doOnNext { localNewsInteractor.saveNews(it.map { remoteConverter.convert(it) }) }
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe({
                    viewState.hideLoading()
                    reloadNewsFromDatabase()
                }, {
                    viewState.hideLoading()
                    viewState.showError("Во время синхронизации возникла ошибка")
                }))
    }

}