package ru.mk.newsreader.ui.newslist

import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ProgressBar
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ru.mk.newsreader.R
import ru.mk.newsreader.adapter.base.DefaultAdapter
import ru.mk.newsreader.adapter.item.Item
import ru.mk.newsreader.data.converter.RemoteNewsConverter
import ru.mk.newsreader.data.interactor.LocalNewsInteractor
import ru.mk.newsreader.data.interactor.RemoteNewsInteractor
import ru.mk.newsreader.rx.SchedulersFactory
import ru.mk.newsreader.ui.base.BaseFragment
import ru.mk.newsreader.ui.newslist.delegate.NewsItemDelegate
import ru.mk.newsreader.util.DateFormatter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class NewsListFragment : BaseFragment(), NewsListView {

    @Inject
    lateinit var localNewsInteractor: LocalNewsInteractor
    @Inject
    lateinit var remoteNewsInteractor: RemoteNewsInteractor
    @Inject
    lateinit var schedulers: SchedulersFactory
    @Inject
    lateinit var router: Router
    @Inject
    lateinit var remoteConverter: RemoteNewsConverter
    @Inject
    lateinit var newsViewItemConverter: NewsViewItemConverter
    @Inject
    lateinit var dateFormatter: DateFormatter
    @InjectPresenter
    lateinit var presenter: NewsListPresenter

    private lateinit var toolbar: Toolbar
    private lateinit var progress: ProgressBar
    private lateinit var recyclerView: RecyclerView

    private lateinit var adapter: DefaultAdapter

    @ProvidePresenter
    fun providePresenter(): NewsListPresenter {
        return NewsListPresenter(
                schedulers,
                localNewsInteractor,
                remoteNewsInteractor,
                router,
                remoteConverter,
                newsViewItemConverter)
    }

    override fun getLayoutRes(): Int {
        return R.layout.fragment_news_list
    }

    override fun initControls(v: View) {
        toolbar = v.findViewById(R.id.toolbar)
        progress = v.findViewById(R.id.view_progress)
        recyclerView = v.findViewById(R.id.recycler)
        initToolbar()
        initRecycler()
        initAdapter()
    }

    private fun initToolbar() {
        toolbar.inflateMenu(R.menu.menu_news_list)
        toolbar.setOnMenuItemClickListener {
            return@setOnMenuItemClickListener when (it.itemId) {
                R.id.action_sync -> {
                    presenter.onActionSync()
                    true
                }
                else -> true
            }
        }
    }

    private fun initRecycler() {
        activity?.let {
            recyclerView.layoutManager = LinearLayoutManager(it, LinearLayoutManager.VERTICAL, false)
            recyclerView.setHasFixedSize(true)
        }
    }

    private fun initAdapter() {
        activity?.let {
            adapter = DefaultAdapter()
                    .addDelegate(NewsItemDelegate(it, dateFormatter, {
                        newsId -> presenter.onNewsSelected(newsId)
                    }))
            recyclerView.adapter = adapter
        }
    }

    override fun showTitle() {
        toolbar.setTitle(R.string.app_name)
    }

    override fun showLoading() {
        progress.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progress.visibility = View.GONE
    }

    override fun showError(error: String) {
        view?.let {
            Snackbar.make(it, error, Snackbar.LENGTH_SHORT).show()
        }
    }

    override fun displayItems(items: List<Item>) {
        adapter.clear()
        adapter.addItems(items)
        adapter.notifyDataSetChanged()
    }

}