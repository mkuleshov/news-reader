package ru.mk.newsreader.ui.splash

import com.arellomobile.mvp.MvpView

interface SplashView: MvpView {

    fun showLoading()
    fun hideLoading()

}