package ru.mk.newsreader.ui.newslist

import com.arellomobile.mvp.MvpView
import ru.mk.newsreader.adapter.item.Item

interface NewsListView: MvpView {

    fun showTitle()
    fun showLoading()
    fun hideLoading()
    fun showError(error: String)
    fun displayItems(items: List<Item>)

}