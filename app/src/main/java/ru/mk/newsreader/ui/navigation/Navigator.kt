package ru.mk.newsreader.ui.navigation

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import ru.mk.newsreader.R
import ru.mk.newsreader.ui.Screens
import ru.mk.newsreader.ui.newsdetail.KEY_NEWS_GUID
import ru.mk.newsreader.ui.newsdetail.createNewsDetailFragment
import ru.mk.newsreader.ui.newslist.NewsListFragment
import ru.mk.newsreader.ui.splash.SplashFragment
import ru.terrakok.cicerone.android.SupportFragmentNavigator

class Navigator(private val fragmentManager: FragmentManager) : SupportFragmentNavigator(fragmentManager, R.id.container) {

    override fun createFragment(screenKey: String, data: Any?): Fragment? {
        return when (screenKey) {
            Screens.SPLASH_SCREEN -> SplashFragment()
            Screens.NEWS_LIST_SCREEN -> NewsListFragment()
            Screens.NEWS_DETAILS_SCREEN -> createNewsDetailFragment(getNewsGuid(data))

            else -> null
        }
    }

    private fun getNewsGuid(data: Any?): String {
        data?.let {
            if (data is Bundle) {
                return data.getString(KEY_NEWS_GUID, "")
            }
        }
        return ""
    }

    override fun showSystemMessage(message: String?) {

    }

    override fun exit() {
        fragmentManager.popBackStackImmediate()
    }

}