package ru.mk.newsreader.util

import java.text.SimpleDateFormat
import java.util.*

interface DateFormatter {

    fun format(date: Date): String

}

class DateFormatterImpl: DateFormatter {

    private val DATE_FORMAT = "dd.MM.yyyy"

    private val formatter = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())

    override fun format(date: Date): String {
        return formatter.format(date)
    }

}