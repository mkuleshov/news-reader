package ru.mk.newsreader.adapter.delegate

import android.content.Context
import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.mk.newsreader.adapter.item.Item
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate

abstract class BaseAdapterDelegate<in T : Item, VH : RecyclerView.ViewHolder>(protected val context: Context) : AdapterDelegate<List<Item>>() {

    private var inflater: LayoutInflater = LayoutInflater.from(context)

    override fun isForViewType(items: List<Item>, position: Int): Boolean {
        return isForViewType(items.get(position))
    }

    protected abstract fun isForViewType(item: Item): Boolean

    @LayoutRes
    protected abstract fun getLayoutId(): Int

    protected abstract fun createViewHolder(view: View): VH

    protected abstract fun onBind(item: T, holder: VH)

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = inflater.inflate(getLayoutId(), parent, false)
        return createViewHolder(view)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onBindViewHolder(items: List<Item>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        val item = items.get(position) as T
        val vh = holder as VH
        onBind(item, holder)
    }

}