package ru.mk.newsreader.adapter.base

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import ru.mk.newsreader.adapter.item.Item
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.hannesdorfmann.adapterdelegates3.AdapterDelegatesManager

class DefaultAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var delegatesManager: AdapterDelegatesManager<List<Item>> = AdapterDelegatesManager()
    private var items: MutableList<Item> = ArrayList()

    override fun getItemViewType(position: Int): Int {
        return delegatesManager.getItemViewType(items, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegatesManager.onCreateViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        delegatesManager.onBindViewHolder(items, position, holder)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun clear() {
        items.clear()
    }

    fun addItems(items: List<Item>) {
        this.items.addAll(items)
    }

    fun getItem(position: Int): Item? {
        return if (position >= 0 && position < items.size) {
            items[position]
        } else null
    }

    fun addDelegate(delegate: AdapterDelegate<List<Item>>): DefaultAdapter {
        delegatesManager.addDelegate(delegate)
        return this
    }

}