package ru.mk.newsreader.app

import com.firebase.jobdispatcher.*
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import ru.mk.newsreader.di.DaggerAppComponent
import ru.mk.newsreader.service.sync.SyncJobService
import timber.log.Timber
import java.util.concurrent.TimeUnit


class MainApplication : DaggerApplication() {

    private val SYNC_PERIOD = 5L
    private val SYNC_WINDOW_DURATION = 10L

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        startSyncJob()
    }

    private fun startSyncJob() {
        val dispatcher = FirebaseJobDispatcher(GooglePlayDriver(baseContext))
        val periodicity = TimeUnit.MINUTES.toSeconds(SYNC_PERIOD).toInt()
        val toleranceInterval = TimeUnit.MINUTES.toSeconds(SYNC_WINDOW_DURATION).toInt()
        val job = dispatcher.newJobBuilder()
                .setTag("SYNC_JOB")
                .setService(SyncJobService::class.java)
                .setTrigger(Trigger.executionWindow(periodicity, periodicity + toleranceInterval))
                .setLifetime(Lifetime.FOREVER)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .setRecurring(true)
                .setReplaceCurrent(true)
                .build()
        dispatcher.schedule(job)
    }

}