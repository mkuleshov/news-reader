package ru.mk.newsreader.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.mk.newsreader.di.annotation.ActivityScope
import ru.mk.newsreader.ui.ActivityModule
import ru.mk.newsreader.ui.MainActivity

@Module
abstract class AppModule {

    @ContributesAndroidInjector(modules = [ActivityModule::class])
    @ActivityScope
    abstract fun mainActivity(): MainActivity

}