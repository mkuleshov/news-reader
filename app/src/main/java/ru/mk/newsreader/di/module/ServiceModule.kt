package ru.mk.newsreader.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.mk.newsreader.di.annotation.ServiceScope
import ru.mk.newsreader.service.sync.SyncJobService
import ru.mk.newsreader.service.sync.SyncModule

@Module
abstract class ServiceModule {

    @ContributesAndroidInjector(modules = [SyncModule::class])
    @ServiceScope
    abstract fun syncJobService(): SyncJobService

}