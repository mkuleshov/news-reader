package ru.mk.newsreader.di.module

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import ru.mk.newsreader.data.api.Api
import javax.inject.Singleton


@Module(includes = [RetrofitModule::class])
class RestApiModule {

    @Provides
    @Singleton
    fun provideRestApi(retrofit: Retrofit): Api {
        return retrofit.create<Api>(Api::class.java)
    }

}