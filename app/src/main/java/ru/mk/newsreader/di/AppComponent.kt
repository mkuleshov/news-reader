package ru.mk.newsreader.di

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import ru.mk.newsreader.app.MainApplication
import ru.mk.newsreader.di.module.*
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    DatabaseModule::class,
    NavigationModule::class,
    ContextModule::class,
    PreferencesModule::class,
    HttpClientModule::class,
    RetrofitModule::class,
    RestApiModule::class,
    SchedulersModule::class,
    AndroidSupportInjectionModule::class,
    ServiceModule::class,
    UtilsModule::class
])
interface AppComponent : AndroidInjector<MainApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<MainApplication>()

}