package ru.mk.newsreader.di.module

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import ru.mk.newsreader.data.db.AppDatabase
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "news.db").build()
    }

    @Provides
    @Singleton
    fun provideNewstDao(database: AppDatabase) = database.newsDao()

}