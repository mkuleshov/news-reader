package ru.mk.newsreader.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import ru.mk.newsreader.app.MainApplication
import javax.inject.Singleton

@Module
class ContextModule {

    @Provides
    @Singleton
    fun provideContext(application: MainApplication): Context {
        return application.applicationContext
    }

}