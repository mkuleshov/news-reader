package ru.mk.newsreader.di.module

import dagger.Module
import dagger.Provides
import ru.mk.newsreader.util.DateFormatter
import ru.mk.newsreader.util.DateFormatterImpl
import javax.inject.Singleton

@Module
class UtilsModule {

    @Provides
    @Singleton
    fun provideDateFormatter(): DateFormatter {
        return DateFormatterImpl()
    }

}