package ru.mk.newsreader.di.module

import ru.mk.newsreader.rx.SchedulersFactory
import ru.mk.newsreader.rx.SchedulersFactoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SchedulersModule {

    @Provides
    @Singleton
    fun provideSchedulers(): SchedulersFactory {
        return SchedulersFactoryImpl()
    }

}