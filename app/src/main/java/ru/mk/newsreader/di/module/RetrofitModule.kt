package ru.mk.newsreader.di.module

import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import org.simpleframework.xml.core.Persister
import org.simpleframework.xml.transform.RegistryMatcher
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory
import ru.mk.newsreader.BuildConfig
import ru.mk.newsreader.data.transform.DateTransformer
import java.util.*
import javax.inject.Singleton


@Module(includes = [
    ContextModule::class,
    HttpClientModule::class,
    PreferencesModule::class
])
class RetrofitModule {

    @Provides
    @Singleton
    fun provideRetrofit(builder: Retrofit.Builder, client: OkHttpClient): Retrofit {
        return builder
                .baseUrl(BuildConfig.API_URL)
                .client(client)
                .build()
    }

    @Provides
    @Singleton
    fun provideRetrofitBuilder(converterFactory: Converter.Factory): Retrofit.Builder {
        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(converterFactory)
    }

    @Provides
    @Singleton
    fun provideConverterFactory(): Converter.Factory {
        val m = RegistryMatcher()
        m.bind(Date::class.java, DateTransformer::class.java)
        return SimpleXmlConverterFactory.create(Persister(m))
    }

}